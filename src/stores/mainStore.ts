import appServices from '../services/index'
export interface UserInfoResponseProps{
    token: string
    personalDetails: PersonalDetailsProps 
} 
export interface PersonalDetailsProps{
    name: string
    team: string 
    joinedAt: string
    avatar: string
}  

export function mainStore() { 
    return {
      userInfo: {
        email: '',
        password: '', 
        response: {} as UserInfoResponseProps,
      }, 
      isLoadingTime: false,
      rowsData: [],
      setIsLoadingTime(val: boolean){
        this.isLoadingTime = val
      },
      setRowsData(rows:any){
        for(let row in rows) 
          if(rows[row]['madeDadeline'] === true)
            rows[row]['madeDadeline'] = `true`;
          else if(rows[row]['madeDadeline'] === false)
              rows[row]['madeDadeline'] = `fales`;
            else continue;
        this.rowsData = rows
      }, 
      async verifyLogin (){
          try{  
            let response = await appServices.postRequest({data: {email: this.userInfo.email, password: this.userInfo.password}, key: 'authenticate'}) 
            if(response && response[0] && response[0].token){
              // console.debug('response',response[0])
              let uinfo = {} as UserInfoResponseProps 
                uinfo.token = response[0].token 
                let info = response[0].personalDetails
                uinfo.personalDetails = response[0].personalDetails
                uinfo.personalDetails = {name: info.name,team: info.Team, joinedAt: info.joinedAt, avatar: info.avatar} 
                // console.debug('uinfo',uinfo)
                // console.debug('uinfo',response[0].personalDetails) 
                appServices.saveToLS('@userInfo',`
                  {"avatar": "${uinfo.personalDetails.avatar}","joinedAt": "${uinfo.personalDetails.joinedAt}","team": "${uinfo.personalDetails.team}","name": "${uinfo.personalDetails.name}","token": "${uinfo.token}","email": "${this.userInfo.email}", "password": "${this.userInfo.password}"}`)
              this.userInfo.response.token = response[0].token  
              this.userInfo.response.personalDetails = {name: info.name,team: info.Team, joinedAt: info.joinedAt, avatar: info.avatar}  
              // console.debug('this.userInfo.response',this.userInfo.response)
              // appServices.saveToLS('@userInfo',`
              //   {"avatar": "${this.userInfo.response.personalDetails.avatar}","joinedAt": "${this.userInfo.response.personalDetails.joinedAt}","team": "${this.userInfo.response.personalDetails.team}","name": "${this.userInfo.response.personalDetails.name}","token": "${this.userInfo.response.token}","email": "${this.userInfo.email}", "password": "${this.userInfo.password}"}`)
              return true  
            } 
            return false
          }catch(e){
            console.debug('verifyLogin err',e)
            return false
          } 
      },
      async getRows (){
          try{   
            let response = await appServices.getRequest({data: {token: this.userInfo.response.token}, key: 'info'}) 
            if(response){
              console.debug('response',response) 
              this.setRowsData(response)
              return true  
            } 
            return false
          }catch(e){
            console.debug('getRows err',e)
            return false
          } 
      }
    }
  }
  
  export type TStore = ReturnType<typeof mainStore>