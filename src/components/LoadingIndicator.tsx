import ReactLoading from 'react-loading'
 
const LoadingIndicator = (props: any) => (
    <ReactLoading type={props.type} color={props.color} height={props.height} width={props.width} />
)
 
export default LoadingIndicator