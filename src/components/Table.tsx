import React from 'react'
import styled from 'styled-components'
import { useTable, useSortBy } from 'react-table'  
import { FaArrowCircleDown,FaArrowCircleUp } from 'react-icons/fa'
import { useStore } from '../stores/store'
import { useObserver } from 'mobx-react'
import { GiCancel,GiConfirmed } from 'react-icons/gi'

const Styles = styled.div`
  padding: 1rem;
  max-height: calc(100vh - 140px) ; 
  overflow:scroll;
  table {
    min-width: 90vw;
    border-spacing: 0;
    border: 1px solid black ;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`

function Table(props: any) {
    const { data, columns }:any = props
    
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  }: any = useTable(
    {
      columns,
      data,
    },
    useSortBy
  )

  // We don't want to render all 2000 rows for this example, so cap
  // it at 20 for this use case
  const firstPageRows = rows.slice(0, 20)

  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup: any) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: any) => (
                // Add the sorting props to control sorting. For this example
                // we can add them into the header props
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render('Header')}
                  {/* Add a sort direction indicator */}
                  <span>
                    {column.isSorted
                      ? column.isSortedDesc
                        ? <FaArrowCircleDown className="table-head-icon"/>
                        : <FaArrowCircleUp className="table-head-icon"/>
                      : ''}
                  </span>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {firstPageRows.map(
            (row: any, i: any) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell: any) => {  
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    )
                  })}
                </tr>
              )}
          )}
        </tbody>
      </table>
      <br />
      {/* <div>Showing the first 20 results of {rows.length} rows</div> */}
    </>
  )
}

function MemoTable() {
  const store = useStore()   
  const columns = React.useMemo(
    () => [
      {
        Header: 'Name',
        columns: [
          {
            Header: 'Id',
            accessor: 'id',
          },
          {
            Header: 'Name',
            accessor: 'name',
          }, 
          {
            Header: 'Score',
            accessor: 'score',
          },
          {
            Header: 'Duration In Days',
            accessor: 'durationInDays',
          },
          {
            Header: 'Bugs Count',
            accessor: 'bugsCount',
          },
          {
            Header: 'Made Dadeline',
            accessor: 'madeDadeline',
          },
        ],
      },
    ],
    []
  )
  const table = useObserver(()=>{

    return (
      <Styles className="table-styles">
        <Table columns={columns} data={store.rowsData} />
      </Styles>
    )

  })
  
  return table
}

export default MemoTable