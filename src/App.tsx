import './App.css'  
import React, { useState } from 'react'
import { BrowserRouter, Route, Switch, withRouter} from 'react-router-dom' 
import { StoreProvider } from './stores/store'
import LoginScreen from './screens/Login' 
import InfoScreen from './screens/Info'   
const Switcher = withRouter(({ location, setDarkState, darkState }: any) => {  
  return ( 
              <Switch location={location}>
                <Route path={`/`} component={()=><LoginScreen /> } exact /> 
                <Route path={`/info`} component={()=><InfoScreen /> } />
              </Switch> )
})
 
function App() {
  return (   
    <StoreProvider>  
        <BrowserRouter> 
           <Switcher/>
        </BrowserRouter>   
    </StoreProvider>
  )
} 


export default App
