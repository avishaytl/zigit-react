import '../App.css';   
import { FaRegEye, FaRegEyeSlash } from 'react-icons/fa';
import { useRef, useState } from 'react';  
import { useHistory } from 'react-router-dom'; 
import styled from 'styled-components';  
import LazyImage from '../components/LazyImage';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { useStore } from '../stores/store';
import appServices from '../services/index';
import {useSpring, animated} from 'react-spring'; 
import LoadingIndicator from '../components/LoadingIndicator';
import { useObserver } from 'mobx-react';
import { useEffect } from 'react';

const lang = {
    Email: 'Email',
    Password: 'Password',
    SignIn: 'Sign In',
    Errors:{
        errorMsgEmail: 'Check your email and try agian!',
        errorMsgPasswordLength: 'Check your password length and try agian!',
        errorMsgPasswordCapitalCase: 'Check if password include A-Z & 0-1 and try agian!'
    }
}

interface UserLoginProps{
  position: string; // left, right, mid  
  isVerifyLogin: boolean;
  imgSrc: string; 
  colors: object;
  onLoginPress: ()=> void; 
  onChangeValue: (val: string, type: string)=>void;
}

interface UserInputProps{
  value: string;
  type: string;
  userRefs: any; 
  onLoginPress: ()=> void;
  onChangeValue: (val: string, type: string)=>void;
}

function Input(props: UserInputProps){ 
  const { value, type, userRefs, onLoginPress, onChangeValue } = props
  const [isHiddenEye, setHiddenEye] = useState(true) 
  return <> 
    <input 
        onKeyDown={(e)=>{
            if (e.key === 'Enter')
              if(type === `email` && userRefs[`password`])
                userRefs[`password`].current.focus();
              else 
                onLoginPress()
        }} 
        ref={userRefs[type]} 
        defaultValue={value} 
        onChange={(e)=>{
            onChangeValue(e.target.value, type) 
        }}  
        autoFocus={type === `email`}  
        type={type === `email` || !isHiddenEye ? `email` : `password`} />
    {type === `password` && (!isHiddenEye ? <FaRegEye onClick={()=>setHiddenEye(!isHiddenEye)}/> : <FaRegEyeSlash onClick={()=>setHiddenEye(!isHiddenEye)}/>)}
  </>
} 

function VerifyLoginErrorMsg ({ on, child }:any) { 
    const props = useSpring({ opacity: on ? 1 : 0, from: { opacity: on ? 0 : 1 } })
    return  <animated.div style={props}>  
                <ErrorMsg>
                    {child}   
                </ErrorMsg>
          </animated.div> 
}; 

function LoginUserModal(props: UserLoginProps){ 
  const { colors, position, imgSrc, isVerifyLogin, onLoginPress, onChangeValue } = props
  const store = useStore()
  const userRefs = {
    email: useRef(null),
    password: useRef(null),
  }; 
  const modal = useObserver(()=>{ 
    return(  
        <UserModal position={position} className={`login-user-modal`}>   
            <LazyImage image={{
                  container: {width:'200px',height:'190px'},
                  alt: 'Login Logo.',
                  src: imgSrc,
              }}/>  
            <UserInputView>
              <UserInputLabel>{lang.Email}</UserInputLabel>
              <UserInput>
                <Input onChangeValue={onChangeValue} onLoginPress={onLoginPress} userRefs={userRefs} value={store.userInfo.email} type={`email`}/> 
              </UserInput> 
            </UserInputView>
            <UserInputView>
              <UserInputLabel>{lang.Password}</UserInputLabel>
              <UserInput> 
                <Input onChangeValue={onChangeValue} onLoginPress={onLoginPress} userRefs={userRefs} value={store.userInfo.password} type={`password`}/> 
              </UserInput> 
            </UserInputView> 
            {<VerifyLoginErrorMsg on={isVerifyLogin} child={appServices.getLoginErrorMsg(lang.Errors)}/> }
            <LoginButton colors={colors} onClick={onLoginPress}>
              {store.isLoadingTime ? <LoginButtonIndicator>
                  <LoadingIndicator type={`balls`} color={`#fff`} height={'10%'} width={'10%'}/>
              </LoginButtonIndicator> : <LoginButtonText>{lang.SignIn}</LoginButtonText>}
            </LoginButton>
        </UserModal>
    ) 
  })
  return modal
}

function Login(){    
    const [modalPosition, setModalPosition] = useState('right') // ~ Change User Login Modal Position  
    const [isVerifyLogin, setVerifyLogin] = useState(false)
    const colors = {primary: '#93d5e3',secondary:'#008ca4', tertiary: '#1e2c4c'}
    const history = useHistory()
    const store = useStore()
    const navigateToMain = () => {
      history.push('/info')
    };  
    useEffect(()=>{
        async function getUserInfo(){  
            // await appServices.removeFromLSO('@userInfo');
            let userInfo = await appServices.getFromLS('@userInfo');
            if(userInfo)
                setUserInfo(userInfo)
        }
        async function setUserInfo(userInfo: any){
            let info = await JSON.parse(userInfo)
            store.userInfo.email = info.email
            store.userInfo.password = info.password 
            store.userInfo.response.token = info.token
            store.userInfo.response.personalDetails = {name: info.name,team: info.team, joinedAt: info.joinedAt, avatar: info.avatar}
            console.debug('login userInfo',store.userInfo.response)  
            setVerifyLogin(false)
            store.setIsLoadingTime(!store.isLoadingTime) 
                setTimeout(() => {
                    navigateToMain()
                    store.setIsLoadingTime(!store.isLoadingTime)
                }, 2500);  
        }
        getUserInfo()
    })
    return(  
        <div>  
          <LoginBackgroundView modalPosition={modalPosition} className={`login-back-view`}>
            <LazyImage image={{
                container: { width:'100vw', height:'100vh' },
                alt: 'Login Background.',
                src: appServices.backgroundImage,
            }}/>
            <LoginUserModal 
                colors={colors}
                position={modalPosition}  
                isVerifyLogin={isVerifyLogin}
                imgSrc={`https://zigit.co.il/wp-content/uploads/2020/08/logoas.svg`} 
                onChangeValue={(val: string, type: string)=>{
                    switch(type){
                        case 'email': store.userInfo.email = val;
                            break;
                        case 'password': store.userInfo.password = val;
                            break; 
                        default:{

                        }
                    }
                }}
                onLoginPress={async()=>{
                    if(!store.isLoadingTime)
                        if(appServices.validateUserInfo(store.userInfo.email, store.userInfo.password)){
                            setVerifyLogin(false)
                            store.setIsLoadingTime(!store.isLoadingTime)
                            if(await store.verifyLogin())
                                setTimeout(() => {
                                    navigateToMain(); 
                                    store.setIsLoadingTime(!store.isLoadingTime)
                                }, 2500); 
                        }else setVerifyLogin(true)
                }} 
              />
          </LoginBackgroundView> 
        </div>   
    )
};
const UserInputView = styled.div` 
    width: 100%;
    height: 80px; 
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center; 
    margin-top: 2px;
    margin-bottom: 2px; 
` 
const UserInput = styled.div` 
    width: 100%;
    height: 50px;
    padding-left: 15px;
    padding-right: 15px;
    background-color: #ececec; 
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start; 
    border-radius: 8px;  
    transition: all ease-in-out .1s;
    border: solid 1px #cecece;
    :hover{
    transform: scale(1.02); 
    -webkit-box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.05);
    -moz-box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.05);
    box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.05); 
    }
` 
const UserInputLabel = styled.label`  
    display: block;
    position: relative; 
    margin-bottom: 3px;
    font-size: 16px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
`   
const ErrorMsg = styled.p`
    font-weight: bold;
    min-height:16px;
    width: 100%;
    text-align: center;
    font-size: 12px;
    color: red; 
`
const LoginButtonIndicator = styled.div` 
    width:100%;
    height:30px;
    padding-bottom:35px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`
const LoginButton = styled.div<any>` 
    width: 100%;
    height: 50px;
    margin-top: 50px;
    background-image: linear-gradient(to right, ${props => props.colors.primary},${props => props.colors.secondary});
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    border-radius: 8px;
    -webkit-box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.25);
    -moz-box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.25);
    box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.25); 
    transition: all ease-in-out .1s;
    :hover{
        transform: scale(1.02);  
    }
`
const LoginButtonText = styled.p` 
    color: #ececec;
    font-weight: bold;
    font-size: 18px;
    cursor: default;
` 

const UserModal = styled.div<any>` 
    width: 450px;
    height: 50vh;
    background: #ffffffc0;
    position: absolute;
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    -webkit-box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.25);
    -moz-box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.25);
    box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.25);
    max-width: 90vw;
    min-height: 550px;
    max-height: 100vh;
    padding-left: 50px; 
    padding-right: 50px; 
    ${props => props.position === 'mid' ? `` : props.position === `left` ? `left: 8vw` : `right: 8vw;`}
` 

const LoginBackgroundView = styled.div<any>` 
    flex:1;
    max-width: 100vw;
    height: 100vh;  
    max-height: 100vh;  
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat; 
    min-width: 100vw;
    display: flex;
    flex-direction: column; 
    align-items: ${props => props.modalPosition === 'mid' ? 'center' : props.modalPosition === 'left' ? 'flex-start' : 'flex-end'};
    justify-content: center; 
`

export default Login;