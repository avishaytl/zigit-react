import '../App.css';  
import React, { useEffect, useState } from 'react';     
import { useStore } from '../stores/store'; 
import appServices from '../services';
import LazyImage from '../components/LazyImage';
import styled from 'styled-components';
import { FaRegUser } from 'react-icons/fa';
import Table from '../components/Table';

function Main(props: any) {  
    const store = useStore()   
    const [isReady,setIsReady] = useState(false);
    async function getUserInfo(){   
        let userInfo = await appServices.getFromLS('@userInfo');
        if(userInfo)
            setUserInfo(userInfo)
    }
    console.debug('store.userInfo.response',store.userInfo,Object.values(store.userInfo.response))  
    async function setUserInfo(userInfo: any){ 
        let info = await JSON.parse(userInfo); 
        store.userInfo.email = info.email; 
        store.userInfo.password = info.password;  
        store.userInfo.response.token = info.token;
        store.userInfo.response.personalDetails = {name: info.name,team: info.team, joinedAt: info.joinedAt, avatar: info.avatar}; 
        console.debug('info userInfo',store.userInfo.response)  
        setIsReady(!isReady)
    } 
    if(!store.userInfo.email)
        getUserInfo() 
    useEffect(()=>{ 
        async function getRows(){    
            if(store.userInfo.email)
                setRows()
        }
        async function setRows(){ 
            let res = await store.getRows() 
        }
        getRows() 
    })
    if(store.userInfo.response)
        return( <div>  
                    <LazyImage image={{
                        container: { width:'100vw', height:'100vh' ,position: 'absolute',zIndex: -1},
                        alt: 'Login Background.',
                        src: appServices.backgroundImage,
                    }}/>  
                    <UserHeaderInFo>
                        <HeaderBar>
                            <LazyImage image={{
                                container: {width:'200px',height:'190px',position: 'absolute', right: 10},
                                alt: 'Zigit Logo.',
                                src: `https://zigit.co.il/wp-content/uploads/2020/08/logoas.svg`,
                            }}/>    
                            <UserInfo>
                                <LazyImage image={{
                                    container: { width:'100%', height:'100%' },
                                    alt: 'User Avatar.',
                                    src: store.userInfo.response && store.userInfo.response.personalDetails && store.userInfo.response.personalDetails.avatar ? store.userInfo.response.personalDetails.avatar : '',
                                }}/>   
                            </UserInfo>
                            <UserName>{store.userInfo.response && store.userInfo.response.personalDetails && store.userInfo.response.personalDetails.name ? store.userInfo.response.personalDetails.name : ''}
                                <UserDate> 
                                    {store.userInfo.response && store.userInfo.response.personalDetails && store.userInfo.response.personalDetails.joinedAt ? store.userInfo.response.personalDetails.joinedAt : ''}
                                </UserDate>
                            </UserName>
                   
                        </HeaderBar>         
                        <UserTeamTitle>  
                            {store.userInfo.response && store.userInfo.response.personalDetails && store.userInfo.response.personalDetails.team ? store.userInfo.response.personalDetails.team : ''}
                        </UserTeamTitle>
                    </UserHeaderInFo>
                    <TableView>
                        <Table/>
                    </TableView>
            </div>)
    return( <div>  
                <LazyImage image={{
                    container: { width:'100vw', height:'100vh' },
                    alt: 'Login Background.',
                    src: appServices.backgroundImage,
                }}/>  
                <NotFoundView>Go Back To Login And Sign In!</NotFoundView>
        </div>)
}

const NotFoundView = styled.div` 
    width: 100vw;
    height: 100vh; 
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;  
    position: absolute;
    top:0;
` 
const UserHeaderInFo = styled.div` 
    width: 100vw;
    height: 100px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;  
    position: fixed;
    top:40px;
    background: #fff; 
    padding: 5px;
    -webkit-box-shadow: 0px 0px 5px 0px #a0a0a0;
    -moz-box-shadow: 0px 0px 5px 0px #a0a0a0;
    box-shadow: 0px 0px 5px 0px #a0a0a0;   
` 
const TableView = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;   
    padding-top: 140px; 
`
const HeaderBar = styled.div` 
    width: 100%;
    height: 100%; 
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;   
` 
const UserInfo = styled.div<any>` 
    overflow: hidden;
    width: 80px;
    height: 80px;
    border-radius: 50px; 
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;  
    image-background: url(${props => props.img}); 
    background-position: center;
    background-size: auto;
    background-repeat: no-repeat; 
    -webkit-box-shadow: 0px 0px 5px 0px #3a3a3a;
    -moz-box-shadow: 0px 0px 5px 0px #3a3a3a;
    box-shadow: 0px 0px 5px 0px #3a3a3a; 
` 
const UserName = styled.h2` 
    margin: 10px;
    color: #000;
    font-weight: bold; 
`
const UserDate = styled.p`  
    color: #000;  
    font-size: 12px;
    text-align: center;
    font-weight: 200;
`
const UserTeamTitle = styled.p` 
    margin: 10px;
    font-weight: bold;
    font-size: 14px;
    color: #000; 
    position: absolute;
    top:0;
`

export default Main;