
// import namor from 'namor'

interface GetRequestProps{
    data: any
    key: string 
}

interface PostRequestProps{
    data: any
    key: string 
}
 
enum ErrorType {
    EMAIL = 'EMAIL',
    CAPITAL = 'CAPITAL',
    LENGTH = 'LENGTH'
}

export default class AppServices {
    baseUrl = 'https://private-052d6-testapi4528.apiary-mock.com'
    backgroundImage = 'https://presolu.com/soft/background.png'
    errorType: string | null = null
    buildGetRequest = (data: any) => {
        let bodyRequest = ""
        for (let key in data) {
            bodyRequest += "&" + key + "=" + data[key]
        }
        return bodyRequest
    }
    
    buildPostRequest = (data: any) => {
        let bodyRequest = ""
        let count = 0
        for (let key in data) {
            if (count++ > 0)
                bodyRequest += "&"
            bodyRequest += key + "=" + data[key]
        }
        return bodyRequest
    }
    
    getRequest = async (params: GetRequestProps): Promise<any> => { 
        const { data, key } = params
        let headers = new Headers()  
        if(data.token){ 
          headers.set("Authorization", `Bearer ${data.token}`) 
          delete data.token
        }
        const geturl = `${this.baseUrl}/${key}${this.buildGetRequest(data)}`
    
        let result = null
    
        try {
            let response = await fetch(geturl, { method: 'GET', headers: headers })
            if (response.ok) 
                return await response.json()
            else 
                return await response.text()
                    
        }
        catch (e) {
            console.log("getRequest err", e)            
        } 
        return result
    }
    
    postRequest = async (params: PostRequestProps): Promise<any> => {  
        const { data, key } = params
        let body = this.buildPostRequest(data)
        let headers = new Headers()   
        const postUrl = `${this.baseUrl}/${key}`
        let result = null 
        try {
            let response = await fetch(postUrl, {
                method: 'POST', headers: headers, body: JSON.stringify(body)
            })
            if (response.ok) 
                return await response.json()
            else
                return await response.text()
        }
        catch (e) {
            console.debug('postRequest err',e)
        } 
        return result
    } 

    validateUserInfo(email: string, password: string){
        const re1 = /^(([^<>()[\]\\.,:\s@"]+(\.[^<>()[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        // const re2 = new RegExp(/[A-Z]+/)  
        if(!re1.test(String(email).toLowerCase())){
            this.errorType = ErrorType.EMAIL 
            return false
        } 
        if(password.length < 8){
            this.errorType = ErrorType.LENGTH
            return false
        }
        if(!password.match(/\d/) || !password.match(/^[a-z0-9]*[A-Z]+[A-Z0-9]*$/)) {
            this.errorType = ErrorType.CAPITAL
            return false
        } 
        return true
    } 
    
    getLoginErrorMsg(messages: any){
      let msg = ''
      switch(this.errorType){
          case ErrorType.EMAIL: msg = messages.errorMsgEmail
          break
          case ErrorType.CAPITAL: msg = messages.errorMsgPasswordCapitalCase 
          break
          case ErrorType.LENGTH:  msg = messages.errorMsgPasswordLength
          break
          default:{

          }
      }
      return msg
    }

    getFromLS(key: string) {
        let ls:any = {}
        if (global.localStorage) {
          try {
            ls = JSON.parse(global.localStorage.getItem("rgl-7") as string) || {}
          } catch (e) { 
              console.debug('getFromLS err',e)
          }
        }
        return ls[key]
      }
      
    removeFromLSO(key?: string) { 
        if (global.localStorage) {
          try {
              global.localStorage.removeItem("rgl-7") 
          } catch (e) {
            console.debug('removeFromLSO err',e) 
          }
        } 
      }
      
    saveToLS(key: string, value: string) {
        if (global.localStorage) {
          global.localStorage.setItem(
            "rgl-7",
            JSON.stringify({
              [key]: value
            })
          )
        }
      }
       
      makeData(...lens: any):any{
        const namor = require("namor")
        const range = (len: number) => {
          const arr = []
          for (let i = 0 ; i < len ; i++) {
            arr.push(i)
          }
          return arr
        }
        
        const newPerson = () => {
          const statusChance = Math.random()
          return {
            id: namor.generate({ words: 1, numbers: 0 }),
            name: namor.generate({ words: 1, numbers: 0 }),
            score: Math.floor(Math.random() * 30),
            durationInDays: Math.floor(Math.random() * 100),
            bugsCount: Math.floor(Math.random() * 100),
            madeDadeLine:
              statusChance > 0.66
                ? 'relationship'
                : statusChance > 0.33
                ? 'complicated'
                : 'single',
          }
        }
         
        const makeDataLevel = (depth = 0):any => {
        const len = lens[depth]
            return range(len).map(d => {
                return {
                ...newPerson(),
                subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined,
                }
            })
        }
    
        return makeDataLevel() 
        
      }
}